﻿using System;
using System.Net.Mail;

namespace AdvHlth.UrgentCareFormsHandler
{
    [Serializable]
    public sealed class MailMessagePlus : MailMessage
    {
        public MailMessagePlus()
        {
            SerializableAttachments = new SerializableAttachmentCollection();
            ToPlus = CcPlus = BccPlus = new SerializableMailAddressCollection();
        }

        public SerializableMailAddress SerializableMailAddress { get; set; }

        public SerializableAttachmentCollection SerializableAttachments { get; }

        public SerializableMailAddressCollection SerializableMailAddressCollection { get; set; }

        public SerializableMailAddressCollection ToPlus { get; }

        public SerializableMailAddress FromPlus { get; }

        public SerializableMailAddressCollection CcPlus { get; }

        public SerializableMailAddressCollection BccPlus { get; }
    }
}
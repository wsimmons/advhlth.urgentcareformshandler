﻿using System;

namespace AdvHlth.UrgentCareFormsHandler
{
    public class UrgentCareExportFile : EmailEntity
    {
        public string SftpUrgentCareServername { get; set; }
        public string SftpUrgentCareServerlocation { get; set; }
        public string IsSftp { get; set; }
		public string IsPdf { get; set; }
		public string IsActive { get; set; }
        public string FileName { get; set; }
		public string FullAssessment { get; set; }
		public Guid UcLogId { get; set; }
        public Guid HraResponseId { get; set; }
    }
}
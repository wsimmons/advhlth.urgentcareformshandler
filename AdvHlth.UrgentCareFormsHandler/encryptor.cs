﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace AdvHlth.UrgentCareFormsHandler
{
    public class Encryptor
    {
        // Internal value of the phrase used to generate the secret key
        private string _Phrase = "";

        /// <value>Set the phrase used to generate the secret key.</value>
        public string Phrase
        {
            set
            {
                this._Phrase = value;
                this.GenerateKey(value);
            }
        }

        // Internal initialization vector value to
        // encrypt/decrypt the first block
        private byte[] _IV;

        // Internal secret key value
        private byte[] _Key;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="SecretPhrase">Secret phrase to generate key</param>
        public Encryptor(string SecretPhrase)
        {
            this.Phrase = SecretPhrase;
        }

        /// <summary>
        /// Encrypt the given value with the Rijndael algorithm.
        /// </summary>
        /// <param name="EncryptValue">Value to encrypt</param>
        /// <returns>Encrypted value. </returns>
        public byte[] Encrypt(string strValue)
        {
            CryptoStream encryptStream = null;       // Stream used to encrypt
            RijndaelManaged rijndael = null;         // Rijndael provider
            ICryptoTransform rijndaelEncrypt = null; // Encrypting object
            MemoryStream memStream = new MemoryStream(); // Stream to contain
                                                         // data

            byte[] EncryptValue = this.ConvertStringToByteArray(strValue);

            try
            {
                if (EncryptValue.Length > 0)
                {
                    // Create the crypto objects
                    rijndael = new RijndaelManaged();
                    rijndael.Key = this._Key;
                    rijndael.IV = this._IV;
                    rijndaelEncrypt = rijndael.CreateEncryptor();
                    encryptStream = new CryptoStream(memStream, rijndaelEncrypt, CryptoStreamMode.Write);

                    // Write the encrypted value into memory
                    encryptStream.Write(EncryptValue, 0, EncryptValue.Length);
                    encryptStream.FlushFinalBlock();

                    // Retrieve the encrypted value and return it
                    byte[] aReturn = memStream.ToArray();
                    //return( Convert.ToBase64String(memStream.ToArray()) );
                    return aReturn;
                    //return this.ConvertByteArrayToString(aReturn);
                }
                else
                {
                    //byte[] errorReturn = { 0x0 };
                    //return errorReturn;
                    return null;
                }
            }
            finally
            {
                if (rijndael != null) rijndael.Clear();
                if (rijndaelEncrypt != null) rijndaelEncrypt.Dispose();
                if (memStream != null) memStream.Close();
            }
        }

        /// <summary>
        /// Decrypt the given value with the Rijndael algorithm.
        /// </summary>
        /// <param name="EncryptValue">Value to decrypt</param>
        /// <returns>Decrypted value. </returns>
        public byte[] Decrypt(byte[] DecryptValue)
        {
            CryptoStream decryptStream = null;       // Stream used to decrypt
            RijndaelManaged rijndael = null;   // Rijndael provider
            ICryptoTransform rijndaelDecrypt = null; // Decrypting object
            MemoryStream memStream = new MemoryStream(); // Stream to contain
                                                         // data

            //byte[] DecryptValue = this.ConvertStringToByteArray(strEncryptedValue);

            try
            {
                if (DecryptValue.Length > 0)
                {
                    // Create the crypto objects
                    rijndael = new RijndaelManaged();
                    rijndael.Key = this._Key;
                    rijndael.IV = this._IV;
                    rijndaelDecrypt = rijndael.CreateDecryptor();
                    decryptStream = new CryptoStream(memStream, rijndaelDecrypt, CryptoStreamMode.Write);

                    // Write the encrypted value into memory
                    decryptStream.Write(DecryptValue, 0, DecryptValue.Length);
                    decryptStream.FlushFinalBlock();

                    // Retrieve the encrypted value and return it
                    byte[] aReturn = memStream.ToArray();
                    return aReturn;
                }
                else
                {
                    return null;
                }
            }
            finally
            {
                if (rijndael != null) rijndael.Clear();
                if (rijndaelDecrypt != null) rijndaelDecrypt.Dispose();
                if (memStream != null) memStream.Close();
            }
        }

        /*****************************************************************
		 * Generate an encryption key based on the given phrase.  The
		 * phrase is hashed to create a unique 32 character (256-bit)
		 * value, of which 24 characters (192 bit) are used for the
		 * key and the remaining 8 are used for the initialization
		 * vector (IV).
		 *
		 * Parameters:  SecretPhrase - phrase to generate the key and
		 * IV from.
		 *
		 * Return Val:  None
		 ***************************************************************/

        private void GenerateKey(string SecretPhrase)
        {
            // Initialize internal values
            this._Key = new byte[24];
            this._IV = new byte[16];

            // Perform a hash operation using the phrase.  This will
            // generate a unique 32 character value to be used as the key.
            byte[] bytePhrase = Encoding.ASCII.GetBytes(SecretPhrase);
            SHA384Managed sha384 = new SHA384Managed();
            sha384.ComputeHash(bytePhrase);
            byte[] result = sha384.Hash;

            // Transfer the first 24 characters of the hashed value to the key
            // and the remaining 8 characters to the intialization vector.
            for (int loop = 0; loop < 24; loop++) this._Key[loop] = result[loop];
            for (int loop = 24; loop < 40; loop++) this._IV[loop - 24] = result[loop];
        }

        public byte[] ConvertStringToByteArray(string stringToConvert)
        {
            return (new UTF8Encoding()).GetBytes(stringToConvert);
        }

        public string ConvertByteArrayToString(byte[] aBytes)
        {
            System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding(); // new correct
            return enc.GetString(aBytes);
        }
    }
}
﻿using AdvHlth.UrgentCareFormsHandler.DAT.Entities;
using AdvHlth.UrgentCareFormsHandler.DAT.Repositories.Concrete;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace AdvHlth.UrgentCareFormsHandler
{
    public static class UrgentCareFormsHandler
    {
        private static HelperClass _hc;
        private static UnitOfWork _uow;
        private static SftpEntity _sftpEntity;
        private static MailMessagePlus _message;

		public static void Run()
		{
			_uow = new UnitOfWork();
			_hc = new HelperClass();

			var emailEntity = new EmailEntity();

			var urgentCareConfigRecords = _uow.UrgentCareConfigurationRepository.GetWhere(_ => _.IsActive == "1").ToList();

			foreach (var urgentCareConfigRecord in urgentCareConfigRecords)
			{
				try
				{
					//var urgentcareConfigTable = _uow.UrgentCareConfigurationRepository.GetWhere(_ => _.CampaignID == urgentCareConfigRecord.CampaignID
					//&& _.MemberState == urgentCareConfigRecord.MemberState);

					List<UrgentCareExportFile> urgentCareExportFiles = null;

					if (_hc.Accounts.Contains(urgentCareConfigRecord.AccountID)) //For VAP we are sending UC forms based on Metro Regions 
					{
						urgentCareExportFiles = GetUrgentCareExportFiles(urgentCareConfigRecord.CampaignID, urgentCareConfigRecord.MemberState, urgentCareConfigRecord.MetroRegion);
					}
					else
					{
						urgentCareExportFiles = GetUrgentCareExportFiles(urgentCareConfigRecord.CampaignID, urgentCareConfigRecord.MemberState);
					}

					if (urgentCareExportFiles.Any())
					{
						_message = new MailMessagePlus();

						foreach (var urgentCareFile in urgentCareExportFiles)
						{
							//Upload Urgent/Emergent form 
							if (urgentCareFile.IsSftp == "1" && _hc.IsDedicatedServer())
								HandleSftpOperations(urgentCareFile, urgentCareFile.FileName);	
							
							//Upload Full Assessment
							if (urgentCareFile.IsSftp == "1" && urgentCareFile.IsPdf == "1" && _hc.IsDedicatedServer() && !String.IsNullOrWhiteSpace(urgentCareFile.FullAssessment))
								HandleSftpOperations(urgentCareFile, urgentCareFile.FullAssessment);

							//Attach UC Form
							if (urgentCareFile.IsAttachment == "1")
								EmailHelper.AttachFilestoEmail(_message, urgentCareFile.FileName);
							
							//Attach Full PDF
							if (urgentCareFile.IsAttachment == "1" && urgentCareFile.IsPdf == "1" && !String.IsNullOrWhiteSpace(urgentCareFile.FullAssessment))
								EmailHelper.AttachFilestoEmail(_message, urgentCareFile.FullAssessment);

							//Db Operations 
							UpdateHraResponse(urgentCareFile.HraResponseId);
							UpdateUcLog(urgentCareFile.UcLogId);
						}

						CreateEmailEntity(emailEntity, urgentCareExportFiles.First());

						if (_hc.IsDedicatedServer())
						{
							BuildAndSendEmail(emailEntity);

							AddEmailLog(emailEntity);
						}
					}
				}
				catch (Exception ex)
				{
					HelperClass.LogErrorMessage(ex);
					continue;
				}
			}
		}

        private static void HandleSftpOperations(UrgentCareExportFile urgentCareFile, string fileName)
        {
            var sftpHelper = new SftpHelper();
            _sftpEntity = sftpHelper.SetSftpEntity(urgentCareFile.SftpUrgentCareServername);

            var sc = default(SftpClient);

			if (!String.IsNullOrWhiteSpace(_sftpEntity.PrivateKeyFile))
			{
				PrivateKeyFile pk;
				PrivateKeyFile[] pkArray = null;

				if (!String.IsNullOrWhiteSpace(_sftpEntity.SftpUserPassword))
					pk = new PrivateKeyFile(@"\\pwahclfs1\web\websites\mms.advancehlth.com\database\Tessellate\id_rsa", _sftpEntity.SftpUserPassword);
				else
					pk = new PrivateKeyFile(@"\\pwahclfs1\web\websites\mms.advancehlth.com\database\Tessellate\id_rsa");

				pkArray = new PrivateKeyFile[] { pk };

				sc = new SftpClient(_sftpEntity.Host, _sftpEntity.SftpUserName, pkArray);
			}
			else
			{

				if (_sftpEntity.Port != null && _sftpEntity.Port > 0)
					sc = new SftpClient(_sftpEntity.Host, (int)_sftpEntity.Port, _sftpEntity.SftpUserName, _sftpEntity.SftpUserPassword);
				else
					sc = new SftpClient(_sftpEntity.Host, _sftpEntity.SftpUserName, _sftpEntity.SftpUserPassword);

			}

            if (sc != default(SftpClient))
            {
                sc.Connect();
                var sftpLogID = Guid.NewGuid();
                AddNewSftpLogEntry(sftpLogID, SftpActionEnum.Connect);
                _sftpEntity.TargetPath = urgentCareFile.SftpUrgentCareServerlocation;
                _sftpEntity.FileName = fileName;
                sftpLogID = Guid.NewGuid();
                AddNewSftpLogEntry(sftpLogID, SftpActionEnum.Put);
                sftpHelper.PutFile(sc, _sftpEntity, true);
                UpdateSftpLogEntry(sftpLogID);
                sc.Disconnect();
                sftpLogID = Guid.NewGuid();
                AddNewSftpLogEntry(sftpLogID, SftpActionEnum.Disconnect);
            }
        }

        private static void AddEmailLog(EmailEntity emailEntity)
        {
            _uow.EmailLogRepository.Add(new EmailLog()
            {
                ID = Guid.NewGuid(),
                Created = DateTime.Now,
                Type = "Urgent Care Automated Email",
                Subject = emailEntity.EmailSubject,
                Email = $"To={emailEntity.EmailTo}|CC={emailEntity.EmailCC}"
            });
            _uow.Save();
        }

        private static void UpdateUcLog(Guid ucLogId)
        {
            var ucLogEntity = _uow.UcLogRepository.GetByID(ucLogId);
            if (ucLogEntity != null)
            {
                ucLogEntity.UcStatus = "Exported to CM";
                _uow.UcLogRepository.Update(ucLogEntity);
                _uow.Save();
            }
        }

        private static void UpdateHraResponse(Guid hraResponseId)
        {
            var hraResponseEntity = _uow.HraResponseRepository.GetByID(hraResponseId);
            if (hraResponseEntity != null)
            {
                hraResponseEntity.SftpUrgentCareDate = DateTime.Now;
                _uow.HraResponseRepository.Update(hraResponseEntity);
                _uow.Save();
            }
        }

        private static void CreateEmailEntity(EmailEntity emailEntity, UrgentCareExportFile urgentCareExportFile)
        {
            emailEntity.EmailSubject = string.Format(urgentCareExportFile.EmailSubject, DateTime.Now.Date.ToString("d"));
            emailEntity.EmailTo = urgentCareExportFile.EmailTo;
            emailEntity.EmailCC = urgentCareExportFile.EmailCC;
            emailEntity.EmailBCC = urgentCareExportFile.EmailBCC;
            emailEntity.EmailBody = urgentCareExportFile.EmailBody;
            emailEntity.EmailFrom = urgentCareExportFile.EmailFrom;
            emailEntity.WebApiURL = ConfigurationManager.AppSettings["emailServiceUrl"];
            emailEntity.IsAttachment = urgentCareExportFile.IsAttachment;
        }

		private static List<UrgentCareExportFile> GetUrgentCareExportFiles(Guid campaignId, string memberState, string region = null)
		{
			var regionParam = default(SqlParameter);
			if (region != null)
			{
				regionParam = new SqlParameter("@Region", region);
			}
			if (regionParam != default(SqlParameter))
			{
				return _uow.DbContext.Database.SqlQuery<UrgentCareExportFile>("GetUrgentCMFilestoExportByCampaignId @CampaignId, @MemberState, @Region",
					new object[]
					{
					new SqlParameter("@CampaignId", campaignId),
					new SqlParameter("@MemberState", memberState),
					regionParam
									}).ToList();
			}
			else
			{
				return _uow.DbContext.Database.SqlQuery<UrgentCareExportFile>("GetUrgentCMFilestoExportByCampaignId @CampaignId, @MemberState",
	                new object[]
	                {
					new SqlParameter("@CampaignId", campaignId),
					new SqlParameter("@MemberState", memberState)
					}).ToList();
			}
		}

        private static void AddNewSftpLogEntry(Guid sftpLogID, SftpActionEnum action)
        {
            _uow.SftpLogRepository.Add(new SftpLog()
            {
                ID = sftpLogID,
                Server = _sftpEntity.TargetServer,
                Action = action.ToString(),
                LocalName = (_sftpEntity.FileName != null && action != SftpActionEnum.Disconnect) ? _sftpEntity.FileName : null,
                RemoteName = (_sftpEntity.TargetPath != null && _sftpEntity.FileName != null && action != SftpActionEnum.Disconnect) ?
                $"{_sftpEntity.TargetPath}/{Path.GetFileName(_sftpEntity.FileName)}/" : null,
                UserID = "9496A6CE-5EDD-4306-9923-B22B00ACDD32",
                Created = DateTime.Now
            });
            _uow.Save();
        }

        private static void UpdateSftpLogEntry(Guid sftpLogID)
        {
            var sftpLogEntry = _uow.SftpLogRepository.GetByID(sftpLogID);
            if (sftpLogEntry != null)
            {
                sftpLogEntry.ConfirmDt = DateTime.Now;
                _uow.SftpLogRepository.Update(sftpLogEntry);
                _uow.Save();
            }
        }

        private static async void BuildAndSendEmail(EmailEntity emailEntity)
        {
            if (emailEntity.EmailSubject.Length > 50)
                emailEntity.EmailSubject = emailEntity.EmailSubject.Substring(0, 50);

            EmailHelper.SetEmailBody(_message, emailEntity);
            EmailHelper.SetEmailAddresses(_message, emailEntity);
            EmailHelper.SetEmailSubject(_message, emailEntity);

            await EmailHelper.SendEmail(_message, emailEntity.WebApiURL);
        }
    }
}
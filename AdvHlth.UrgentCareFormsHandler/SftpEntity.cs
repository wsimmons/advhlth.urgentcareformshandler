﻿namespace AdvHlth.UrgentCareFormsHandler
{
    public class SftpEntity
    {
        public string FileName { get; set; }
        public string TargetServer { get; set; }
        public string TargetPath { get; set; }
        public string SftpUserName { get; set; }
        public string SftpUserPassword { get; set; }
        public int? Port { get; set; }
        public string ServerType { get; set; }
        public string Host { get; set; }
		public string PrivateKeyFile { get; set; }
	}
}
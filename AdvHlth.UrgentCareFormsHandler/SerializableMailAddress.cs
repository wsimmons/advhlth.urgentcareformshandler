﻿using System;

namespace AdvHlth.UrgentCareFormsHandler
{
    [Serializable]
    public class SerializableMailAddress
    {
        public SerializableMailAddress()
        {
        }

        public SerializableMailAddress(string address)
        {
            Address = address;
        }

        public string Address { get; set; }
    }
}
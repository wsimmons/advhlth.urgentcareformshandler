﻿namespace AdvHlth.UrgentCareFormsHandler
{
    public enum SftpActionEnum
    {
        Connect,
        Put,
        Disconnect
    }
}
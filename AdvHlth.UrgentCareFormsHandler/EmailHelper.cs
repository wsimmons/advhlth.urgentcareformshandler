﻿using System;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AdvHlth.UrgentCareFormsHandler
{
    public static class EmailHelper
    {
        public static void SetEmailAddresses(MailMessagePlus message, EmailEntity emailEntity)
        {
            if (!string.IsNullOrWhiteSpace(emailEntity.EmailFrom))
                message.From = new MailAddress(emailEntity.EmailFrom);
            else
                throw new Exception("From email address is blank.");

            //Email To Starts
            SetToOrCCAddress(message.To, emailEntity.EmailTo);
            //Email To Ends

            //Email CC Starts
            SetToOrCCAddress(message.CC, emailEntity.EmailCC);
            //Email CC Ends

            if (string.IsNullOrWhiteSpace(emailEntity.EmailBCC)) return;
            message.Bcc.Add(emailEntity.EmailBCC);
        }

        private static void SetToOrCCAddress(MailAddressCollection collection, string entityCollection)
        {
            //if (string.IsNullOrWhiteSpace(emailEntity.EmailTo)) throw new Exception("To email address is blank.");
            if (entityCollection.Contains(";"))
            {
                foreach (string address in entityCollection.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                    //message.To.Add(new MailAddress(address));
                    DecodeDistributionList(collection, address);
            }
            else if (entityCollection.Contains(","))
            {
                foreach (string address in entityCollection.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    //message.To.Add(new MailAddress(address));
                    DecodeDistributionList(collection, address);
            }
            else
                DecodeDistributionList(collection, entityCollection);
        }

        public static void SetEmailBody(MailMessagePlus message, EmailEntity emailEntity) =>
            message.Body = $"{message.Body}{emailEntity.EmailBody}\r\n\r\n\r\nDo not reply to this email address.\r\n\r\n\r\n";

        public static void SetEmailSubject(MailMessagePlus message, EmailEntity emailEntity)
        {
            if (emailEntity.EmailSubject.Contains("{0}"))
                message.Subject = string.Format(emailEntity.EmailSubject, DateTime.Now.ToShortDateString());
            else
                message.Subject = $"{message.Subject}{emailEntity.EmailSubject}";
            emailEntity.EmailSubject = message.Subject;
        }

        public static void AttachFilestoEmail(MailMessagePlus message, string fileName) =>
            message.SerializableAttachments.Add(fileName);

        public static async Task<string> SendEmail(MailMessagePlus message, string WebApiUrl)
        {
            using (var client = new HttpClient() { BaseAddress = new Uri(WebApiUrl) })
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string data = new JavaScriptSerializer().Serialize(message);

                var httpResponse = await client.GetAsync($"{client.BaseAddress}/api/email/send?data={data}");

                return httpResponse.StatusCode.ToString();
            }
        }

        private static void DecodeDistributionList(MailAddressCollection collection, string address)
        {
            if (address == ConfigurationManager.AppSettings["distributionListEmail"])
            {
                AddEmailAddresses(ConfigurationManager.AppSettings["distributionListName"], collection);
            }
            else
            {
                collection.Add(new MailAddress(address));
            }
        }

        private static void AddEmailAddresses(string distributionList, MailAddressCollection collection)
        {
            //string distributionList = "AH Urgent Care";
            if (!string.IsNullOrWhiteSpace(distributionList))
            {
                using (var ctx = new PrincipalContext(ContextType.Domain, ConfigurationManager.AppSettings["domainName"]))
                {
                    using (var list = GroupPrincipal.FindByIdentity(ctx, distributionList))
                    {
                        foreach (var member in list.GetMembers())
                        {
                            if (member.GetType().GetProperty("EmailAddress") != null)
                            {
                                collection.Add(member.GetType().GetProperty("EmailAddress").GetValue(member).ToString());
                            }
                            else
                            {
                                AddEmailAddresses(member.Name, collection);
                            }
                        }
                    }
                }
            }
        }
    }
}
﻿using AdvHlth.UrgentCareFormsHandler.DAT.Repositories.Concrete;
using Renci.SshNet;
using System;
using System.IO;
using System.Linq;

namespace AdvHlth.UrgentCareFormsHandler
{
    public class SftpHelper
    {
        public const string GENERIC_PHRASE = "The True North strong and free";
        private Encryptor _encryptor;

        public string SftpLogUserID { get; internal set; }

        public SftpEntity SetSftpEntity(string serverName)
        {
            try
            {
                _encryptor = new Encryptor("Gotta Catch Em All");

                var sftpConfig = new UnitOfWork().SftpRepository.GetWhere(_ => _.ServerName == serverName).FirstOrDefault();

                // passwords are encrypted and saved as base 64
                string strEncrypted = sftpConfig.PassHash;
                //convert from base 64 to byte array
                byte[] aEncrypted = Convert.FromBase64String(strEncrypted);
                //decrypt byte array
                byte[] aDecrypted = _encryptor.Decrypt(aEncrypted);
                //convert from byte array to utf-8 string
                string decryptedPassword = _encryptor.ConvertByteArrayToString(aDecrypted);

                return new SftpEntity()
                {
                    SftpUserName = sftpConfig.UserID,
                    SftpUserPassword = decryptedPassword,
                    Port = sftpConfig.Port,
                    ServerType = sftpConfig.ServerType,
                    Host = sftpConfig.Url,
                    TargetServer = serverName,
					PrivateKeyFile = sftpConfig.PrivateKeyFile
				};
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PutFile(SftpClient sc, SftpEntity sftpEntity, bool isOverwrite)
        {
            //string strID = logTransaction("put", strLocalFullFileName, strRemoteFullFileName);
            using (var file = File.OpenRead(sftpEntity.FileName))
            {
                try
                {
                    sc.BufferSize = 4 * 1024; //bypass Payload error large files
                    sc.UploadFile(file, sftpEntity.TargetPath + Path.GetFileName(sftpEntity.FileName), isOverwrite);
                }
                catch (Exception err)
                {
                    if (err.Message == "Permission denied")
                    {
                        throw new Exception("Error putting file on server, confirm that you have permission to write to this folder.", err);
                    }
                    if (err.Message == "Failure" && isOverwrite == false)
                    {
                        throw new Exception("Error putting file on server, Confirm file does not already exist", err);
                    }
                    throw new Exception("Other Exception while trying to send file", err);
                }
            }
        }

        ///// <summary>
        ///// Take an xml list of files and put them all up to the ftp server
        ///// </summary>
        ///// <param name="ndFiles"></param>
        ///// <param name="strXPath"></param>
        ///// <param name="strAttributeName"></param>
        ///// <param name="strRemoteDirectory"></param>
        ///// <param name="bOverwrite"></param>
        //public void PutFiles(XmlElement ndFiles, string strXPath, string strAttributeName, string strRemoteDirectory, bool bOverwrite, string strIdColumn, string strStatusTable, string strStatusColumn)
        //{
        //    foreach (XmlElement ndFile in ndFiles.SelectNodes(strXPath))
        //    {
        //        string strFullFileName = ndFile.GetAttribute(strAttributeName);
        //        try
        //        {
        //            FileInfo fi = new FileInfo(strFullFileName);
        //            if (!fi.Exists)
        //            {
        //                throw new Exception("File does not exist");
        //            }
        //            string strFileName = Path.GetFileName(strFullFileName);
        //            PutFile(strFullFileName, strRemoteDirectory + strFileName, bOverwrite);
        //        }
        //        catch (Exception err)
        //        {
        //            throw new Exception("Error putting file on sftp server " + strFullFileName, err);
        //        }
        //    }
        //}
    }
}
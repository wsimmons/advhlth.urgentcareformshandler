﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvHlth.UrgentCareFormsHandler
{
    public class DedicatedServerElement : ConfigurationElement
    {
        [ConfigurationProperty("name", DefaultValue = null, IsRequired = true)]
        public string Name
        {
            get { return this["name"].ToString(); }
            set { this["name"] = value; }
        }
    }
}

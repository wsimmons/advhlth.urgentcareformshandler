﻿using System;
using System.Collections.ObjectModel;

namespace AdvHlth.UrgentCareFormsHandler
{
    [Serializable]
    public class SerializableMailAddressCollection : Collection<SerializableMailAddress>, IDisposable
    {
        private bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~SerializableMailAddressCollection() => Dispose(false);

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                this.Clear();
            }

            _disposed = true;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvHlth.UrgentCareFormsHandler
{
    public class DedicatedServerCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new DedicatedServerElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as DedicatedServerElement).Name;
        }
    }
}

﻿namespace AdvHlth.UrgentCareFormsHandler
{
    public class EmailEntity
    {
        public string EmailFrom { get; set; }
        public string EmailTo { get; set; }
        public string EmailCC { get; set; }
        public string EmailBCC { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public string TargetServer { get; set; }
        public string TargetPath { get; set; }
        public string SftpList { get; set; }
        public string AccountName { get; set; }
        public int? NoofFile { get; set; }
        public string WebApiURL { get; set; }
        public string IsAttachment { get; set; }
    }
}
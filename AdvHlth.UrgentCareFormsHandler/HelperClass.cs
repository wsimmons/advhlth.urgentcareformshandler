﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace AdvHlth.UrgentCareFormsHandler
{
    public class HelperClass
    {
        #region SFTP

        public bool IsDedicatedServer()
        {
            var configSection = ConfigurationManager.GetSection("dedicatedServers") as DedicatedServerSection;

            foreach (DedicatedServerElement server in configSection.DedicatedServers)
            {
                if (server.Name == Environment.MachineName) return true;
            }

            return false;
        }

        #endregion SFTP

        #region IO Operations

        public static void LogErrorMessage(Exception ex)
        {
            string newLine = Environment.NewLine;
			using (var sw = new StreamWriter($@"\\pwahclfs1\web\AH Batch Job\Urgent Care Forms Handler\ErrorLog.txt", true))
			{
				sw.WriteLine($"{ DateTime.Now}{newLine}{ex.Message}{newLine}");
			}
		}

		#endregion IO Operations

		#region Account
		public List<Guid> Accounts => new Guid[] {
			Guid.Parse("29E72A1A-39E3-4101-80CE-495BB214F7C3")
		}.ToList();

		#endregion
	}
}
﻿using System;
using System.Collections.ObjectModel;

namespace AdvHlth.UrgentCareFormsHandler
{
    [Serializable]
    public class SerializableAttachmentCollection : Collection<string>, IDisposable
    {
        private bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~SerializableAttachmentCollection() => Dispose(false);

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                this.Clear();
            }

            _disposed = true;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvHlth.UrgentCareFormsHandler
{
    public class DedicatedServerSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsRequired = true, IsDefaultCollection = true)]
        public DedicatedServerCollection DedicatedServers
        {
            get { return (DedicatedServerCollection)this[""]; }
            set { this[""] = value; }
        }

    }
}

﻿using System;
using System.IO;
using System.Threading;

namespace AdvHlth.UrgentCareFormsHandler
{
    internal class Program
    {
        private static void Main(string[] args)
        {
			try
            {
				UrgentCareFormsHandler.Run();
			}
            catch (Exception ex)
            {
				HelperClass.LogErrorMessage(ex);

				var eventLog = new System.Diagnostics.EventLog();
				eventLog.WriteEntry(ex.Message, System.Diagnostics.EventLogEntryType.Error);


			}
            finally
            {
				Thread.Sleep(10000);
            }
        }
    }
}
﻿using AdvHlth.UrgentCareFormsHandler.DAT.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace AdvHlth.UrgentCareFormsHandler.DAT
{
    public class UrgentCareDbContext : DbContext
    {
        public UrgentCareDbContext() : base("UrgentCareDbContext")
        {
        }

        public DbSet<UrgentCareConfiguration> UrgentCareConfigurations { get; set; }
        public DbSet<Sftp> Sftps { get; set; }
        public DbSet<SftpLog> SftpLogs { get; set; }
        public DbSet<HraResponse> HraResponses { get; set; }
        public DbSet<UcLog> UcLogs { get; set; }
        public DbSet<EmailLog> EmailLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) =>
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AdvHlth.UrgentCareFormsHandler.DAT.Entities
{
    public class HraResponse
    {
        [Key]
        public Guid ID { get; set; }

        public DateTime? SftpUrgentCareDate { get; set; }
    }
}
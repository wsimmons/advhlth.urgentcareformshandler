﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AdvHlth.UrgentCareFormsHandler.DAT.Entities
{
    public class Sftp
    {
        [Key]
        public Guid ID { get; set; }

        public string ServerName { get; set; }
        public string Url { get; set; }
        public string DefaultPath { get; set; }
        public string PassHash { get; set; }
        public int? Port { get; set; }
        public string UserID { get; set; }
        public string DevelopmentUrl { get; set; }
        public string ServerType { get; set; }
		public string PrivateKeyFile { get; set; }
    }
}
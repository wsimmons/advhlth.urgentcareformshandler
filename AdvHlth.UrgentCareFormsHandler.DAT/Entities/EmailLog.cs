﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AdvHlth.UrgentCareFormsHandler.DAT.Entities
{
    public class EmailLog
    {
        [Key]
        public Guid ID { get; set; }

        public DateTime Created { get; set; }
        public string Type { get; set; }
        public string Subject { get; set; }
        public string Email { get; set; }
    }
}
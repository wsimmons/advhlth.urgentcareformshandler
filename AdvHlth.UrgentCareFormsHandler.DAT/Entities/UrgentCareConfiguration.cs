﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AdvHlth.UrgentCareFormsHandler.DAT
{
    public class UrgentCareConfiguration
    {
        [Key]
        [Required]
        public Guid ID { get; set; }

        [Required]
        public Guid AccountID { get; set; }

        [Required]
        public Guid CampaignID { get; set; }

        [MaxLength(2, ErrorMessage = "MemberState Max Length 2 Characters")]
        public string MemberState { get; set; }

        [Required]
        [MaxLength(500, ErrorMessage = "SftpUrgentCareServerName Max Length 500 Characters")]
        public string SftpUrgentCareServerName { get; set; }

        [Required]
        [MaxLength(500, ErrorMessage = "SftpUrgentCareServerLocation Max Length 500 Characters")]
        public string SftpUrgentCareServerLocation { get; set; }

        [MaxLength(500, ErrorMessage = "EmailFrom Max Length 500 Characters")]
        public string EmailFrom { get; set; }

        [MaxLength(1000, ErrorMessage = "EmailTo Max Length 1000 Characters")]
        public string EmailTo { get; set; }

        [MaxLength(1000, ErrorMessage = "EmailCc Max Length 1000 Characters")]
        public string EmailCc { get; set; }

        [MaxLength(500, ErrorMessage = "EmailBcc Max Length 500 Characters")]
        public string EmailBcc { get; set; }

        [MaxLength(250, ErrorMessage = "EmailSubject Max Length 250 Characters")]
        public string EmailSubject { get; set; }

        [MaxLength(8000, ErrorMessage = "EmailText Max Length 8000 Characters")]
        public string EmailText { get; set; }

        [Required]
        [MaxLength(1, ErrorMessage = "IsActive Max Length 1 Character")]
        public string IsActive { get; set; }

        [MaxLength(1, ErrorMessage = "IsAttachment Max Length 1 Character")]
        public string IsAttachment { get; set; }

        [MaxLength(1, ErrorMessage = "IsSftp Max Length 1 Character")]
        public string IsSftp { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdatedBy { get; set; }
		public string Metro { get; set; }
		public string MetroRegion { get; set; }
	}
}
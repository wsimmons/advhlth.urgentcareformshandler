﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AdvHlth.UrgentCareFormsHandler.DAT.Entities
{
    public class SftpLog
    {
        [Key]
        public Guid ID { get; set; }

        public string Server { get; set; }
        public string Action { get; set; }
        public string LocalName { get; set; }
        public string RemoteName { get; set; }
        public string UserID { get; set; }
        public DateTime Created { get; set; }
        public DateTime? ConfirmDt { get; set; }
    }
}
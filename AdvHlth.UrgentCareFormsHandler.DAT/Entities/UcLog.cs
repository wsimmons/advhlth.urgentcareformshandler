﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AdvHlth.UrgentCareFormsHandler.DAT.Entities
{
    public class UcLog
    {
        [Key]
        public Guid ID { get; set; }

        public string UcStatus { get; set; }
    }
}
﻿using AdvHlth.UrgentCareFormsHandler.DAT.Entities;
using System;

namespace AdvHlth.UrgentCareFormsHandler.DAT.Repositories.Concrete
{
    public class UnitOfWork : IDisposable
    {
        private UrgentCareDbContext _database;
        private GenericRepository<UrgentCareConfiguration> _urgentCareConfigRepo;
        private GenericRepository<Sftp> _sftpRepo;
        private GenericRepository<SftpLog> _sftpLogRepo;
        private GenericRepository<EmailLog> _emailLogRepo;
        private GenericRepository<UcLog> _ucLogRepo;
        private GenericRepository<HraResponse> _hraResponseRepo;
        private bool _disposed;

        public UnitOfWork()
        {
            _database = new UrgentCareDbContext();
            _urgentCareConfigRepo = UrgentCareConfigurationRepository;
            _sftpRepo = SftpRepository;
            _sftpLogRepo = SftpLogRepository;
            _emailLogRepo = EmailLogRepository;
            _ucLogRepo = UcLogRepository;
            _hraResponseRepo = HraResponseRepository;
        }

        public UrgentCareDbContext DbContext
        {
            get { return _database; }
        }

        public GenericRepository<UrgentCareConfiguration> UrgentCareConfigurationRepository =>
            _urgentCareConfigRepo ?? new GenericRepository<UrgentCareConfiguration>(_database);

        public GenericRepository<Sftp> SftpRepository => _sftpRepo ?? new GenericRepository<Sftp>(_database);
        public GenericRepository<SftpLog> SftpLogRepository => _sftpLogRepo ?? new GenericRepository<SftpLog>(_database);
        public GenericRepository<EmailLog> EmailLogRepository => _emailLogRepo ?? new GenericRepository<EmailLog>(_database);
        public GenericRepository<UcLog> UcLogRepository => _ucLogRepo ?? new GenericRepository<UcLog>(_database);
        public GenericRepository<HraResponse> HraResponseRepository => _hraResponseRepo ?? new GenericRepository<HraResponse>(_database);

        public void Save() => _database.SaveChanges();

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _database.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
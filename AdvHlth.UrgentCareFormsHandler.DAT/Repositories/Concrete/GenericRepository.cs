﻿using AdvHlth.UrgentCareFormsHandler.DAT.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace AdvHlth.UrgentCareFormsHandler.DAT.Repositories.Concrete
{
    public class GenericRepository<T> : IRepository<T> where T : class
    {
        private UrgentCareDbContext _context;
        private DbSet<T> _entitySet;

        public GenericRepository(UrgentCareDbContext context)
        {
            _context = context;
            _entitySet = _context.Set<T>();
        }

        public T GetByID(Guid id) => _entitySet.Find(id);

        public IEnumerable<T> GetAll() => _entitySet;

        public IEnumerable<T> GetWhere(Expression<Func<T, bool>> where) => _entitySet.Where(where);

        public void Add(T entity) => _entitySet.Add(entity);

        public void Update(T entity) => _context.Entry(entity).State = EntityState.Modified;

        public void Delete(T entity) => _entitySet.Remove(entity);

        public void Save() => _context.SaveChanges();
    }
}
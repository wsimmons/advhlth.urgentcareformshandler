﻿using AdvHlth.UrgentCareFormsHandler.DAT.Repositories.Concrete;

namespace AdvHlth.UrgentCareFormsHandler.DAT.Repositories.Abstract
{
    public interface IUnitOfWork<T> where T : class
    {
        GenericRepository<T> UrgentCareConfigurationRepository { get; set; }
        GenericRepository<T> SftpRepository { get; set; }
        GenericRepository<T> SftpLogRepository { get; set; }
        GenericRepository<T> EmailLogRepository { get; set; }
        GenericRepository<T> UcLogRepository { get; set; }
        GenericRepository<T> HraResponseRepository { get; set; }

        void Save();
    }
}
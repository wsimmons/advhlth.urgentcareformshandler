﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace AdvHlth.UrgentCareFormsHandler.DAT.Repositories.Abstract
{
    public interface IRepository<T> where T : class
    {
        T GetByID(Guid id);

        IEnumerable<T> GetAll();

        IEnumerable<T> GetWhere(Expression<Func<T, bool>> where);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);

        void Save();
    }
}